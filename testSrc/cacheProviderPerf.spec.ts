// import { last, range } from "@vlr/array-tools";
// import { wait } from "@vlr/async-tools";
// import * as jscache from "js-cache";
// import { CacheProvider } from "../src";

// const size = 250000;

// describe("jsCache", function (): void {
//   it("should work", async function (): Promise<void> {
//     // arrange
//     const target = new jscache();
//     const items = range(size).map(i => ({ i, s: i.toString() }));

//     // act
//     const insertStart = new Date().getTime();
//     items.forEach(i => target.set(i.s, i.i, 50));

//     const insertEnd = new Date().getTime();
//     console.log("insertion " + (insertEnd - insertStart));
//     while (target.get(last(items).s) != null) {
//       await wait(30);
//       console.log("waited");
//     }
//     const noItem = new Date().getTime();
//     console.log("deletion " + (noItem - insertEnd));

//     // assert

//   });
// });

// describe("plainCache", function (): void {
//   it("should work", async function (): Promise<void> {
//     // arrange
//     const target = new CacheProvider({ timeoutPrecision: 50 });
//     const items = range(size).map(i => ({ i, s: i.toString() }));

//     // act
//     const insertStart = new Date().getTime();
//     items.forEach(i => target.set(i.s, i.i, 50));

//     const insertEnd = new Date().getTime();
//     console.log("insertion " + (insertEnd - insertStart));
//     while (target.has(last(items).s)) {
//       await wait(30);
//       console.log("waited");
//     }
//     const noItem = new Date().getTime();
//     console.log("deletion " + (noItem - insertEnd));
//     // assert
//   });
// });




import { CacheProvider } from "../src";
import { expect } from "chai";
import { wait } from "@vlr/async-tools";
import { range, last } from "@vlr/array-tools";

describe("CacheProvider", function (): void {
  it("should return value if sooner than timeout", async function (): Promise<void> {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 40 });
    const key = "1";
    const value = 1;

    // act
    target.set(key, value, 40);
    await wait(20);
    const has = target.has(key);
    const result = target.get(key);

    // assert
    expect(result).equals(value);
    expect(has).equals(true);
  });

  it("should return value if sooner than timeout precision", async function (): Promise<void> {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 40 });
    const key = "1";
    const value = 1;

    // act
    target.set(key, value, 10);
    await wait(20);
    const has = target.has(key);
    const result = target.get(key);

    // assert
    expect(result).equals(value);
    expect(has).equals(true);
  });

  it("should return null if later than timeout", async function (): Promise<void> {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 20 });
    const key = "1";
    const value = 1;

    // act
    target.set(key, value, 20);
    await wait(40);
    const has = target.has(key);
    const result = target.get(key);

    // assert
    expect(result == null).equals(true);
    expect(has).equals(false);
  });

  it("should expire items earlier", async function (): Promise<void> {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 50 });
    const items = range(2000);

    // act
    items.forEach(i => target.set(i.toString(), i, 200));
    items.forEach(i => target.set(i.toString(), i, 20));

    await wait(50);
    // assert
    expect(target.has(last(items).toString())).equals(false);
  });

  it("should expire items later", async function (): Promise<void> {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 10 });
    const items = range(2000);

    // act
    items.forEach(i => target.set(i.toString(), i, 10));
    items.forEach(i => target.set(i.toString(), i, 60));


    // assert
    await wait(30);
    expect(target.has(last(items).toString())).equals(true);
    await wait(70);
    expect(target.has(last(items).toString())).equals(false);
  });


  it("should delete item", async function (): Promise<void> {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 100 });
    const key = "10";
    // act
    target.set(key, 10, 0);
    target.delete(key);
    const has = target.has(key);
    const result = target.get(key);
    // assert
    expect(has).equals(false);
    expect(result == null).equals(true);
  });

  it("should use second bucket item", async function (): Promise<void> {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 25 });

    // act
    target.set("10", 10, 20);
    target.set("20", 20, 40);
    const has = target.has("20");
    await wait(70);
    const has2 = target.has("20");

    // assert
    expect(has).equals(true);
    expect(has2).equals(false);
  });
});

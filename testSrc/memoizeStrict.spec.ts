import { expect } from "chai";
import { memoizeStrict } from "../src";
import { wait } from "@vlr/async-tools";

const calls = [];

function calledFunc(arg1: number, arg2: string): number {
  calls.push({ arg1, arg2 });
  return arg1 + +arg2;
}

describe("memoizeStrict", function (): void {
  it("should memoize the function result", async function (): Promise<void> {
    // arrange
    calls.length = 0;
    const target = memoizeStrict(calledFunc);
    const arg1 = 1, arg2 = "2";
    const expected = [{ arg1, arg2 }];
    // act
    target(arg1, arg2);
    expect(calls).deep.equals(expected);
    const result = target(arg1, arg2);

    // assert
    expect(calls).deep.equals(expected);
    expect(result).equals(3);
  });

  it("should memoize 2 function results", async function (): Promise<void> {
    // arrange
    calls.length = 0;
    const target = memoizeStrict(calledFunc);

    const arg1 = 1, arg2 = "2";
    const arg11 = 3, arg12 = "4";
    const expected = [{ arg1, arg2 }, { arg1: arg11, arg2: arg12 }];

    // act
    target(arg1, arg2);
    target(arg1, arg2);
    target(arg11, arg12);
    target(arg11, arg12);

    // assert
    expect(calls).deep.equals(expected);
  });

  it("should drop function result after timeout", async function (): Promise<void> {
    // arrange
    calls.length = 0;
    const target = memoizeStrict(calledFunc, { defaultTimeout: 30, timeoutPrecision: 30 });
    const arg1 = 1, arg2 = "2";
    const expected = [{ arg1, arg2 }, { arg1, arg2 }];
    // act
    target(arg1, arg2);
    await wait(60);
    target(arg1, arg2);

    // assert
    expect(calls).deep.equals(expected);
  });
});

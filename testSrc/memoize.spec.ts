import { memoize } from "../src";
import { expect } from "chai";
import { wait } from "@vlr/async-tools";

let called = {};

function calledFunc(arg: number): number {
  called[arg] = (called[arg] || 0) + 1;
  return arg + 10;
}

describe("memoize", function (): void {
  it("should memoize the function result", async function (): Promise<void> {
    // arrange
    called = {};
    const target = memoize(calledFunc);

    // act
    target(1);
    expect(called[1]).equals(1);
    const result = target(1);

    // assert
    expect(called[1]).equals(1);
    expect(result).equals(11);
  });

  it("should memoize 2 function results", async function (): Promise<void> {
    // arrange
    called = {};
    const target = memoize(calledFunc);

    // act
    target(11);
    target(11);
    target(22);
    target(22);

    // assert
    expect(called[11]).equals(1);
    expect(called[22]).equals(1);
  });

  it("should drop function result after timeout", async function (): Promise<void> {
    // arrange
    called = {};
    const target = memoize(calledFunc, { defaultTimeout: 30, timeoutPrecision: 30 });

    // act
    target(33);
    await wait(60);
    target(33);

    // assert
    expect(called[33]).equals(2);
  });
});

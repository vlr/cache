import { series } from "gulp";
import { compileTypescript } from "./parts";
import { preparation } from "./preparation";

export const build = series(
  preparation,
  compileTypescript
);


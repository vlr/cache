import { Version } from "./version";
import * as gitTag from "git-tag";

const regex = /v?(\d\d*).(\d\d*).(\d\d*)/;
export function getGitTagVersion(): Promise<Version> {
  return new Promise<Version>(resolve => {
    gitTag({ localOnly: true })
      .latest(tag => {
        const m = tag.match(regex);
        const version = m ? { major: +m[1], minor: +m[2], patch: +m[3] } : { major: 0, minor: 0, patch: 0 };
        resolve(version);
      });
  });
}

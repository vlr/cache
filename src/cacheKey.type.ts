import { IMap } from "@vlr/map-tools/objectMap";
export type CacheKey = IMap<any> | string | number;

import { nullObject } from "@vlr/object-tools";
import { Cache } from "./cache";
import { CacheSettings } from "./cacheSettings.type";

export function memoize<T extends Function>(func: T, settings?: CacheSettings): T {
  const cache: Cache = new Cache(settings);
  const result: any = function (...args: any[]): any {
    const key = JSON.stringify(args);
    return cache.getSync(key, () => func.apply(null, args));
  };
  return result;
}

export const memoized = memoizeStrict;

export function memoizeStrict<T extends Function>(func: T, settings?: CacheSettings): T {
  let cache: Cache;
  let argsLength: number;
  let actualFunction;

  actualFunction = function initializeCache(): any {
    cache = new Cache(settings);
    argsLength = arguments.length;
    actualFunction = runCache;
    return actualFunction.apply(this, arguments);
  };

  function runCache(): any {
    const key = nullObject();
    let i = argsLength;
    const args = arguments;
    while (i--) { key[i] = args[i]; }
    return cache.getSync(key, () => func.apply(null, args));
  }


  return <any>function (): any {
    return actualFunction.apply(this, arguments);
  };
}

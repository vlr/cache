import { CacheSettings } from "./cacheSettings.type";
import { Expiration } from "./expiration/expiration";
import { KeyMap } from "@vlr/map-tools";
import { CacheKey } from "./cacheKey.type";

export class CacheProvider {
  private expiration: Expiration;
  private store: KeyMap<CacheKey, any>;

  constructor(settings: CacheSettings) {
    this.expiration = new Expiration(this.expire, settings);
    this.store = new KeyMap(null);
  }

  public get = (key: CacheKey) => {
    const val = this.store.get(key);
    return val && val.value;
  }

  public set = (key: CacheKey, value: any, timeout?: number) => {
    const val = { value };
    this.store.addWithKey(val, key);
    this.expiration.setExpiration(key, val, timeout);
  }

  public has = (key: CacheKey) => this.store.has(key);
  public delete = (key: CacheKey) => this.store.delete(key);

  private expire = (key: CacheKey, value: any) => {
    if (this.store.get(key) === value) {
      this.store.delete(key);
    }
  }
}
